import java.util.*;

public class Chapter {
	private int number;
	// For Tan Suo, Model -> monster list
	// For Yu Hun, "BaQiDaShe" -> monster list
	public Map<String, String[]> MONSTERS;
	
	public Chapter (int number) {
		MONSTERS = new TreeMap<>();
		this.number = number;
	}
	
	public int getNumber() { return number; }
	
	public void addMonsterSet(String model, String[] monsters) {
		MONSTERS.put(model, monsters);
	}
}
