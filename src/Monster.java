import java.util.*;

public class Monster {
	public String NAME;
	// level -> {model -> how many}
	public Map<Integer, Map<String, Integer>> TAN_SUO;
	// level -> how many
	public Map<Integer, Integer> YU_HUN;
	
	public Monster(String name) {
		this.NAME = name;
		TAN_SUO = new HashMap<>();
		YU_HUN = new HashMap<>();
	}
	
	public int getTotalOfTanSuoLevel(int level) {
		int total = 0;
		if (TAN_SUO.containsKey(level)) {
			for (Integer i : TAN_SUO.get(level).values()) {
				total += i;
			}
		}
		return total;
	}
}
