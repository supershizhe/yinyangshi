import java.util.*;
import java.util.Map.Entry;
import java.io.*;

public class Main {
	private static final String TanSuo_FILE_NAME = "TanSuo.txt";
	private static final String YuHun_FILE_NAME = "YuHun.txt";
	private static final String MonsterMap_FILE_NAME = "MonsterMap.txt";
	private static final String Result_FILE_NAME = "Result.txt";
	private static final String Mission_FILE_NAME = "Mission.txt";
	private static final int SUSHI_BOSS_EXPECTED = 15;
	
	private static int HIGHEST_TAN_SUO_POSSIBLE = 0;
	private static int HIGHEST_YU_HUN_POSSIBLE = 0;
	private static int HIGHEST_YU_HUN = 0;
	private static int HIGHEST_TAN_SUO = 0;
	private static Map<String, Double> TAN_SUO_RESULT = new TreeMap<>(); // model -> value
	private static Map<String, Double> YU_HUN_RESULT = new TreeMap<>();
	private static List<String> MODEL_VALUE_SORT = new ArrayList<>();
	private static int SUSHI_TAN_SUO = 3;
	private static int SUSHI_YU_HUN = 6;
	private static List<Chapter> tanSuo;
	private static List<Chapter> yuHun;
	private static List<String> rawValuePathData = new ArrayList<>();
	private static boolean useDefault = false;
	
	public static void main(String[] args) throws FileNotFoundException {
		System.out.println("*** 欢迎来到牛逼的Ω悬赏封印解决方案生成器 ***\n");
		@SuppressWarnings("resource")
		Scanner console = new Scanner(System.in);
		tanSuo = processTanSuoSource();
		yuHun = processYuHunSource();
		useDefault(console);
		PrintStream config = null;
		if (!useDefault) {
			config = new PrintStream(new File("Config"));
		}
		if (useDefault || !hasUpdates(tanSuo.size(), yuHun.size(), console)) {
			Map<String, Integer> missions = getMissionsData();
			if (useDefault) {
				console = new Scanner(new File("Config"));
			}
			getConsoleOptions(console, config);
			Map<String, Monster> monsterMap = getProceedMonsterMap();
			exportMonsterMap(monsterMap);
			PrintStream output = new PrintStream(new File(Result_FILE_NAME));
			reportPlain(monsterMap, missions, output);
			calculateBestRoute(monsterMap, missions, output);
			showBlackTechAndEnding();
		}
	}
	
	private static void useDefault(Scanner console) {
		System.out.print(">>> 是否使用上次使用的御魂、探索层数设置(Y/N)：");
		String answer = console.nextLine();
		useDefault = answer.startsWith("Y") || answer.startsWith("y");
	}
	
	// true for use, false for edit
	private static boolean hasUpdates(int tanSuo, int yuHun, Scanner console) {
		System.out.print(">>> 目前资料更新至探索" + tanSuo + "层，御魂" + yuHun
				+ "层。是否为所知最新资源(Y/N)：");
		String ask = console.next();
		if (ask.startsWith("Y") || ask.startsWith("y")) {
			System.out.print(">>> 请根据格式用文件编辑软件更新根目录下的“" + Mission_FILE_NAME
					+ "”文件后输入字符串继续(任意字符串，例如“我没有SSR”)：");
			String anything = console.next();
			return false;
		}
		System.out.println("*** 请根据格式用文件编辑软件更新根目录下的“" + YuHun_FILE_NAME
				+ "”或“TanSuo”文件后重新运行程序。***");
		return true;
	}
	
	private static List<Chapter> processTanSuoSource() throws FileNotFoundException {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(new File(TanSuo_FILE_NAME));
		List<Chapter> result = new ArrayList<Chapter>();
		while (input.hasNextLine()) {
			String line = input.nextLine();
			if (line.length() <= 0 || line.startsWith("#")) {
				continue;
			}
			int level = Integer.parseInt(line);
			Chapter chapter = new Chapter(level);
			HIGHEST_TAN_SUO_POSSIBLE = level;
			while (input.hasNextLine()) {
				String nextMonsterSetString = input.nextLine();
				if (nextMonsterSetString.trim().equals("-")) {
					break;
				}
				String[] model_set = nextMonsterSetString.split("：");
				String[] monsters = model_set[1].split("，");
				chapter.addMonsterSet(model_set[0], monsters);
			}
			result.add(chapter);
		}
		return result;
	}
	
	private static List<Chapter> processYuHunSource() throws FileNotFoundException {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(new File(YuHun_FILE_NAME));
		List<Chapter> result = new ArrayList<Chapter>();
		while (input.hasNextLine()) {
			String nextMonsterSetString = input.nextLine();
			if (nextMonsterSetString.length() <= 0 || nextMonsterSetString.startsWith("#")) {
				continue;
			}
			String[] level_set = nextMonsterSetString.split("：");
			int level = Integer.parseInt(level_set[0]);
			Chapter chapter = new Chapter(level);
			HIGHEST_YU_HUN_POSSIBLE = level;
			String[] monsters = level_set[1].split("，");
			chapter.addMonsterSet("八岐大蛇", monsters);
			result.add(chapter);
		}
		return result;
	}
	
	private static Map<String, Integer> getMissionsData() throws FileNotFoundException {
		Map<String, Integer> result = new TreeMap<>();
		@SuppressWarnings("resource")
		Scanner input = new Scanner(new File(Mission_FILE_NAME));
		while (input.hasNextLine()) {
			String missionString = input.nextLine();
			if (missionString.length() <= 0 || missionString.startsWith("#")) {
				continue;
			}
			String[] mission = missionString.split("x");
			result.put(mission[0], Integer.parseInt(mission[1]));
		}
		return result;
	}

	private static void getConsoleOptions(Scanner console, PrintStream output) {
		if (!useDefault) {
			System.out.print(">>> 可打最高探索层数(数字1-" + HIGHEST_TAN_SUO_POSSIBLE + ")：");
		}
		HIGHEST_TAN_SUO = console.nextInt();
		if (!useDefault) {
			output.println(HIGHEST_TAN_SUO);
			System.out.print(">>> 是否包括御魂副本(Y/N)：");
		}
		String useTanSuo = console.next();
		if (!useDefault) {
			output.println(useTanSuo);
		}
		if (useTanSuo.startsWith("Y") || useTanSuo.startsWith("y")) {
			if (!useDefault) {
				System.out.print(">>> 御魂是否与人组队(Y/N)：");
			}
			String zuDui = console.next();
			if (!useDefault) {
				output.println(zuDui);
			}
			if (zuDui.startsWith("Y") || zuDui.startsWith("y")) {
				SUSHI_YU_HUN = 4;
			}
			if (!useDefault) {
				System.out.print(">>> 可打最高御魂层数(数字1-" + HIGHEST_YU_HUN_POSSIBLE + ")：");
			}
			HIGHEST_YU_HUN = console.nextInt();
			if (!useDefault) {
				output.println(HIGHEST_YU_HUN);
			}
		}
	}
	
	private static Map<String, Monster> getProceedMonsterMap() {
		Map<String, Monster> result = new TreeMap<>();
		setMonsterMapWithTanSuo(result);
		setMonsterMapWithYuHun(result);
		return result;
	}
	
	private static void setMonsterMapWithTanSuo(Map<String, Monster> result) {
		for (Chapter chapter : tanSuo) {
			int level = chapter.getNumber();
			for (Entry<String, String[]> model_monsters : chapter.MONSTERS.entrySet()) {
				String model = model_monsters.getKey();
				String[] monsters = model_monsters.getValue();
				for (String monster : monsters) {
					Monster monsterObj = new Monster(monster);
					if (result.containsKey(monster)) {
						monsterObj = result.get(monster);
					} else {
						result.put(monster, monsterObj);
					}
					if (monsterObj.TAN_SUO.containsKey((Integer) level)) {
						if (monsterObj.TAN_SUO.get(level).containsKey(model)) {
							int curNum = monsterObj.TAN_SUO.get(level).get(model);
							monsterObj.TAN_SUO.get(level).put(model, curNum + 1);
						} else {
							monsterObj.TAN_SUO.get(level).put(model, 1);
						}
					} else {
						Map<String, Integer> model_num = new TreeMap<>();
						model_num.put(model, 1);
						monsterObj.TAN_SUO.put(level, model_num);
					}
				}
			}
		}
	}
	
	private static void setMonsterMapWithYuHun(Map<String, Monster> result) {
		for (Chapter chapter : yuHun) {
			int level = chapter.getNumber();
			for (String monster : chapter.MONSTERS.get("八岐大蛇")) {
				Monster monsterObj = new Monster(monster);
				if (result.containsKey(monster)) {
					monsterObj = result.get(monster);
				} else {
					result.put(monster, monsterObj);
				}
				if (monsterObj.YU_HUN.containsKey((Integer) level)) {
					int curNum = monsterObj.YU_HUN.get(level);
					monsterObj.YU_HUN.put(level, curNum + 1);
				} else {
					monsterObj.YU_HUN.put(level, 1);
				}
			}
		}
	}
	
	private static void exportMonsterMap(Map<String, Monster> monsterMap) throws FileNotFoundException {
		PrintStream output = new PrintStream(new File(MonsterMap_FILE_NAME));
		for (Monster monsterObj : monsterMap.values()) {
			reportMonsterInfo(output, monsterObj, Integer.MAX_VALUE, Integer.MAX_VALUE, false);
		}
	}
	
	private static void reportPlain(Map<String, Monster> monsterMap,
			Map<String, Integer> missions, PrintStream output) throws FileNotFoundException {
		System.out.println("\n======= 妖怪出没地点 =======");
		output.println("\n======= 妖怪出没地点 =======");
		String intro = "* 格式为“-- 副本：总数量。模型(数量)”\n" +
				"* 比如“-- 探索2层：共4只。涂壁1(1) 赤舌2(3)”意思为“探索2层中共有4只需要击杀的式神。其中第一个图壁模型包含1个，第二个赤舌模型包含3个。”\n";
		System.out.println(intro);
		output.println(intro);
		for (String monster : missions.keySet()) {
			reportMonsterInfo(System.out, monsterMap.get(monster),
					HIGHEST_TAN_SUO, HIGHEST_YU_HUN, true);
			reportMonsterInfo(output, monsterMap.get(monster),
					HIGHEST_TAN_SUO, HIGHEST_YU_HUN, true);
		}
	}
	
	private static void reportMonsterInfo(PrintStream output, Monster monsterObj,
			int maxTanSuo, int maxYuHun, boolean addToBestRouteCalculation) {
		output.println(monsterObj.NAME + "：");
		for (Entry<Integer, Map<String, Integer>> entry : monsterObj.TAN_SUO.entrySet()) {
			int level = entry.getKey();
			if (level > maxTanSuo) {
				break;
			}
			output.print("-- 探索" + level + "层：共" + monsterObj.getTotalOfTanSuoLevel(level) + "只。");
			for (Entry<String, Integer> model_num : entry.getValue().entrySet()) {
				output.print(model_num.getKey() + "(" + model_num.getValue() + ") ");
				if (addToBestRouteCalculation) {
					String tanSuoKey = "探索" + level + "层 -- " + model_num.getKey();
					if (TAN_SUO_RESULT.containsKey(tanSuoKey)) {
						if (tanSuoKey.contains("BOSS")) {
							TAN_SUO_RESULT.put(tanSuoKey, TAN_SUO_RESULT.get(tanSuoKey)
									+ 1.0 * model_num.getValue() / SUSHI_BOSS_EXPECTED);
						} else {
							TAN_SUO_RESULT.put(tanSuoKey, TAN_SUO_RESULT.get(tanSuoKey)
									+ 1.0 * model_num.getValue() / SUSHI_TAN_SUO);
						}
					} else {
						if (tanSuoKey.contains("BOSS")) {
							TAN_SUO_RESULT.put(tanSuoKey, 1.0 * model_num.getValue() / SUSHI_BOSS_EXPECTED);
						} else {
							TAN_SUO_RESULT.put(tanSuoKey, 1.0 * model_num.getValue() / SUSHI_TAN_SUO);
						}
					}
				}
			}
			output.println();
		}
		for (Entry<Integer, Integer> entry : monsterObj.YU_HUN.entrySet()) {
			int level = entry.getKey();
			if (level > maxYuHun) {
				break;
			}
			if (addToBestRouteCalculation) {
				String yuHunKey = "御魂" + level + "层";
				if (YU_HUN_RESULT.containsKey(yuHunKey)) {
					YU_HUN_RESULT.put(yuHunKey, YU_HUN_RESULT.get(yuHunKey)
							+ 1.0 * entry.getValue() / SUSHI_YU_HUN);
				} else {
					YU_HUN_RESULT.put(yuHunKey, 1.0 * entry.getValue() / SUSHI_YU_HUN);
				}
			}
			output.println("-- 御魂" + level + "层：共" + entry.getValue() + "只。");
		}
		output.println();
	}
	
	private static void sortBestValues(Map<String, Integer> missions, PrintStream output) {
		for (Entry<String, Double> model_value : TAN_SUO_RESULT.entrySet()) {
			int i = 0;
			while (i < MODEL_VALUE_SORT.size() && TAN_SUO_RESULT.get(MODEL_VALUE_SORT.get(i))
					>= model_value.getValue()) {
				i++;
			}
			MODEL_VALUE_SORT.add(i, model_value.getKey());
		}
		for (Entry<String, Double> model_value : YU_HUN_RESULT.entrySet()) {
			int i = 0;
			while (i < MODEL_VALUE_SORT.size()) {
				double value = 0.0;
				if (YU_HUN_RESULT.containsKey(MODEL_VALUE_SORT.get(i))) {
					value = YU_HUN_RESULT.get(MODEL_VALUE_SORT.get(i));
				} else {
					value = TAN_SUO_RESULT.get(MODEL_VALUE_SORT.get(i));
				}
				if (value < model_value.getValue()) {
					break;
				} else {
					i++;
				}
			}
			MODEL_VALUE_SORT.add(i, model_value.getKey());
		}
		System.out.println("\n===== 击杀价值排行 (个数/体力) =====");
		output.println("\n===== 击杀价值排行 (个数/体力) =====");
		String intro = "* 格式为“副本 -- 模型：式神(数量)”\n" +
				"* 比如“探索3层 -- 帚神2：涂壁(4) 赤舌(5)”意思为“探索3层的第2个帚神模型中，包含4个涂壁和5个赤舌”\n";
		System.out.println(intro);
		output.println(intro);
		for (int i = 0; i < MODEL_VALUE_SORT.size(); i++) {
			String model = MODEL_VALUE_SORT.get(i);
			String modelContent = getMissionContent(model, missions.keySet());
			System.out.println(model + "：" + modelContent);
			output.println(model + "：" + modelContent);
			rawValuePathData.add(model + "：" + modelContent);
		}
	}
	
	private static void calculateBestRoute(Map<String, Monster> monsterMap,
			Map<String, Integer> missions, PrintStream output) {
		sortBestValues(missions, output);
		samplePlan(missions, output);
	}
	
	private static void samplePlan(Map<String, Integer> missions, PrintStream output) {
		System.out.println("\n\n===== 推荐击杀方案 (耗费体力最少或相对很少) =====");
		output.println("\n\n===== 推荐击杀方案 (耗费体力最少或相对很少) =====");
		String intro = "* 格式为“副本 -- 模型：式神(数量)”\n" +
				"* 比如“探索3层 -- 帚神2：涂壁(4) 赤舌(5)”意思为“探索3层的第2个帚神模型中，包含4个涂壁和5个赤舌”\n";
		System.out.println(intro);
		output.println(intro);
		List<String> path = new ArrayList<>();
		Set<String> oldMissions = new TreeSet<>();
		oldMissions.addAll(missions.keySet());
		for (String key : oldMissions) {
			if (missions.get(key) <= 0) {
				missions.remove(key);
			}
		}
		while (!missions.isEmpty()) {
			rawValuePathData = reSort(missions);
			String pathForward = rawValuePathData.get(0);
			path.add(pathForward);
			String[] monsters = pathForward.split("：")[1].split("\\)");
			oldMissions = new TreeSet<>();
			oldMissions.addAll(missions.keySet());
			for (String key : oldMissions) {
				for (String monster : monsters) {
					monster = monster.trim();
					if (monster.contains(key)) {
						int killingNum = Integer.parseInt(
								monster.substring(monster.indexOf("(") + 1));
						missions.put(key, missions.get(key) - killingNum);
						break;
					}
				}
			}
			for (String key : oldMissions) {
				if (missions.get(key) <= 0) {
					missions.remove(key);
				}
			}
		}
		int totalSushi = 0;
		boolean hasBoss = false;
		for (String s : path) {
			System.out.println(s);
			output.println(s);
			if (s.contains("探索")) {
				if (s.contains("BOSS")) {
					totalSushi += SUSHI_BOSS_EXPECTED;
					hasBoss = true;
				} else {
					totalSushi += SUSHI_TAN_SUO;
				}
			} else {
				totalSushi += SUSHI_YU_HUN;
			}
		}
		String sushiConclusion = "* 共需要" + totalSushi + "体力。";
		if (hasBoss) {
			sushiConclusion += "其中包括探索BOSS战。每个BOSS战算作需要" + SUSHI_BOSS_EXPECTED + "体力。";
		}
		System.out.println(sushiConclusion + "\n");
		output.println(sushiConclusion + "\n");
	}
	
	private static List<String> reSort(Map<String, Integer> missions) {
		Map<String, Double> set_value = new TreeMap<>();
		for (String entireLine : rawValuePathData) {
			int total = 0;
			String[] monstersWithNum = entireLine.split("：")[1].split("\\)");
			for (String monster : missions.keySet()) {
				for (String monsterWithNum : monstersWithNum) {
					if (monsterWithNum.contains(monster.trim())) {
						int monsterWithNumTotal = Integer.parseInt(
								monsterWithNum.substring(monsterWithNum.indexOf("(") + 1));
						int monsterTotal = missions.get(monster);
						total += Math.min(monsterWithNumTotal, monsterTotal);
						break;
					}
				}
			}
			int sushi = SUSHI_YU_HUN;
			if (entireLine.contains("探索")) {
				if (entireLine.contains("BOSS")) {
					sushi = SUSHI_BOSS_EXPECTED;
				} else {
					sushi = SUSHI_TAN_SUO;
				}
			}
			double value = 1.0 * total / sushi;
			set_value.put(entireLine, value);
		}
		List<String> result = new ArrayList<>();
		for (Entry<String, Double> lineValue : set_value.entrySet()) {
			int i = 0;
			while (i < result.size() && set_value.get(result.get(i)) >= lineValue.getValue()) {
				i++;
			}
			result.add(i, lineValue.getKey());
		}
		return result;
	}
	
	private static String getMissionContent (String info, Set<String> missions) {
		Map<String, Integer> result = new TreeMap<>();
		String[] monsters = null;
		if (info.contains("探索")) {
			String[] level_model = info.split("--");
			level_model[0] = level_model[0].substring("探索".length(), level_model[0].indexOf("层"));
			Chapter chapter = tanSuo.get(Integer.parseInt(level_model[0]) - 1);
			monsters = chapter.MONSTERS.get(level_model[1].trim());
			
		} else {
			String levelString = info.substring("御魂".length(), info.indexOf("层"));
			Chapter chapter = yuHun.get(Integer.parseInt(levelString) - 1);
			monsters = chapter.MONSTERS.get("八岐大蛇");
		}
		
		for (String monster : monsters) {
			if (missions.contains(monster)) {
				if (result.containsKey(monster)) {
					result.put(monster, result.get(monster) + 1);
				} else {
					result.put(monster, 1);
				}
			}
		}
		
		String resultString = "";
		for (Entry<String, Integer> monster_num : result.entrySet()) {
			resultString += monster_num.getKey() + "(" + monster_num.getValue() + ") ";
		}
		return resultString.trim();
	}
	
	private static void showBlackTechAndEnding() {
		System.out.println("\n===== 以上结果也被写入了根目录下的“" + Result_FILE_NAME + "”文件。"
				+ "完整的怪物分布图被写入了根目录下的“"+ MonsterMap_FILE_NAME + "”文件。 =====");
		System.out.println("\n===== 黑科技刷本信息 =====");
		System.out.println("含有跳跳哥哥、桃花妖和童男等复活流人物的，同群怪里的任意其他怪理论上可以刷超过一次。"
				+ "此为刷本黑科技。具体方式为先攻击不具有复活技能的式神，让可以复活他人的式神对其进行复活从而达到"
				+ "单次体力多次刷怪的效果。如想使用黑科技刷本请查看根目录下的“"+ MonsterMap_FILE_NAME
				+ "”文件中复活流妖怪的位置。");
		System.out.println("\n\n*** 感谢使用牛逼的Ω悬赏封印解决方案生成器 ***");
	}
}
