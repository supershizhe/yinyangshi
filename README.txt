《牛逼的Ω悬赏封印解决方案生成器》说明

===== 版本 =====
1.1.0 (增加了不需compile的版本)
1.1.1 (修改了跳跳犬的名字和一些数据上的bug)

===== 介绍 =====
1. 本程序为解决手游阴阳师中的每日悬赏封印找怪问题。根据不同的悬赏封印总结怪物出现副本，以及提供最佳刷本路线（最省体力完成所有悬赏封印）。
2. 程序为自己自娱自乐的赶工制作。算法速度都没有考虑使用最佳方案。只为能达到目的。仅用来个人娱乐。
3. 任务必须是可以完成的。无法完成的任务包括可用副本中没有找到任务中提到的式神等。具体见“使用步骤-1”。

===== 下载 =====
1. 通过Git：打开terminal或command line（命令提示符），进入到希望下载的页面。git clone https://supershizhe@bitbucket.org/supershizhe/yinyangshi.git
2. 直接下载：在浏览器中打开https://supershizhe@bitbucket.org/supershizhe/yinyangshi.git，点击页面上的下载/Download标志。
3. 其他：请联系“Contact”邮箱

===== 使用步骤 =====
1. 在Mission.txt中填入你今天要做的任务。格式为：“姓名x数量”。其中“x”为字母xyz的x。例如“天邪鬼赤x3”。可以同时有多个任务。
2. Compile/Run Main.java。
    2.1 在command line/terminal中运行
    	2.1.1 把所有txt文件和Config文件移动到与其他java文件同目录下
    	2.1.2 在同目录下输入 javac Main.java; java Main
    2.2 在IDE（eclipse, jGrasp, etc.）中运行：运行Main.java
3. 根据提示操作。结果会发布在屏幕上和Result文件内。完整的怪物分布图会发布在“MonsterMap”文件内。

===== 添加新探索或御魂副本信息 =====
目前收录的信息为探索1-17，御魂1-9。数据为2016年11月9日最新。如需添加新层数或改动现有数据，请在TanSuo和YuHun文件内进行修改。格式见各个文件。

===== Copyright =====
个人娱乐软件。开源。欢迎使用和修改。禁止一切商业用途。传播请打码标明出处（微博@翔爷归来）
For personal use only. Open source. Free to use and updating code is welcome. All business purpose forbidden. Please share with sign for origin (Weibo@翔爷归来)

===== Contact =====
supershizhe@gmail.com
